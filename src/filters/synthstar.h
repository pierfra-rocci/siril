#ifndef SRC_SYNTHSTAR_H_
#define SRC_SYNTHSTAR_H_

gpointer do_synthstar();
gpointer fix_saturated_stars();


#define SYNTHESIZE_GAUSSIAN 0
#define SYNTHESIZE_MOFFAT 1

#endif /* SRC_SYNTHSTAR_H_ */
